package com.udacity.jwdnd.course1.cloudstorage.services;

import com.udacity.jwdnd.course1.cloudstorage.mappers.CredentialMapper;
import com.udacity.jwdnd.course1.cloudstorage.models.Credential;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CredentialService {
    private CredentialMapper credentialMapper;
    private EncryptionService encryptionService;

    private Credential encryptPassword(Credential credential) {
        String key = RandomStringUtils.random(16, true, true);
        credential.setKey(key);
        credential.setPassword(encryptionService.encryptValue(credential.getPassword(), key));
        return credential;
    }

    public Credential decryptPassword(Credential credential) {
        credential.setPassword(encryptionService.decryptValue(credential.getPassword(), credential.getKey()));
        return credential;
    }

    public List<Credential> getAllCredentials(int userid) throws Exception {
        List<Credential> credential = credentialMapper.findByUserId(userid);
        if (credential == null) {
            throw new Exception();
        }
        return credential.stream().map(this::decryptPassword).collect(Collectors.toList());
    }

    public void addCredential(Credential credential, int userid) {
        credentialMapper.insertCredential(encryptPassword(credential), userid);
    }

    public void updateCredential(Credential credential) {
        credentialMapper.updateCredential(encryptPassword(credential));
    }

    public void deleteCredential(int credentialid) {
        credentialMapper.deleteCredential(credentialid);
    }
}
