package com.udacity.jwdnd.course1.cloudstorage.mappers;

import com.udacity.jwdnd.course1.cloudstorage.models.Credential;
import com.udacity.jwdnd.course1.cloudstorage.models.Note;
import org.apache.ibatis.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;

public interface NoteMapper {

    @Select("SELECT * FROM Note WHERE noteid = #{noteid}")
    Note getNoteById(int noteid);

   @Insert("INSERT INTO Note (notetitle, notedescription, userid) VALUES(#{notetitle}," +
            " #{notedescription}, #{userid})")
    @Options(useGeneratedKeys = true, keyProperty = "noteId")
    void insertNote(Note note);

    @Update("UPDATE Note SET notetitle=#{notetitle}, notedescription=#{notedescription}" +
            "WHERE userid=#{userid} AND noteid=#{noteid}")
    void updateNote(Note note);

    @Delete("DELETE FROM Note WHERE noteid = #{noteid}")
    void deleteNote(int noteid);
}