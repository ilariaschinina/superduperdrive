package com.udacity.jwdnd.course1.cloudstorage.controllers;

import com.udacity.jwdnd.course1.cloudstorage.models.Note;
import com.udacity.jwdnd.course1.cloudstorage.models.User;
import com.udacity.jwdnd.course1.cloudstorage.services.NoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/notes")
public class NoteController {
    private NoteService noteService;

    @PostMapping
    public String saveOrUpdateNotes(Authentication authentication, Note note) {
        User user = (User) authentication.getPrincipal();
        note.setUserid(user.get);
        if (note.getNoteid() > 0) {
            noteService.updateNote(note);
        } else {
            noteService.addNote(note);
        }
        return "notes";
    }
}
