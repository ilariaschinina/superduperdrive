package com.udacity.jwdnd.course1.cloudstorage.mappers;

import com.udacity.jwdnd.course1.cloudstorage.models.File;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

public interface FileMapper {

    @Select("SELECT * FROM File WHERE fileId = #{fileId}")
    File getId(int fileId);

    @Insert("INSERT INTO File (fileId, filename, contenttype, filesize, filedata, userid) VALUES(#{fileId}, #{filename}," +
            " #{contenttype}, #{filesize}, #{filedata}, #{userid})")
    @Options(useGeneratedKeys = true, keyProperty = "fileId")
    int insert(File file);

    @Delete("DELETE FROM File WHERE fileId = #{fileId}")
    void delete(int fileId);
}
