package com.udacity.jwdnd.course1.cloudstorage.mappers;

import com.udacity.jwdnd.course1.cloudstorage.models.User;
import org.apache.ibatis.annotations.*;

@Mapper
public interface UserMapper {

    @Select("SELECT * FROM Users WHERE userid = #{userid}")
    User getUserById(int userid);

    @Select("SELECT * FROM Users WHERE username = #{username}")
    User getUserByUsername(String username);

    @Insert("INSERT INTO Users (username, salt, password, firstname, lastname) VALUES(#{username}, #{salt}, #{password}," +
            " #{firstName}, #{lastName})")
    @Options(useGeneratedKeys = true, keyProperty = "userId")
    int insert(User user);

    @Delete("DELETE FROM Users WHERE userid = #{userid}")
    void delete(int userid);

}
