package com.udacity.jwdnd.course1.cloudstorage.services;

import com.udacity.jwdnd.course1.cloudstorage.mappers.NoteMapper;
import com.udacity.jwdnd.course1.cloudstorage.models.Note;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NoteService {
    NoteMapper noteMapper;

    public void addNote(Note note) {
        noteMapper.insertNote(note);
    }

    public void updateNote(Note note) {
        noteMapper.updateNote(note);
    }

    public void deleteNote(int noteid) {
        noteMapper.deleteNote(noteid);
    }

    Note getNoteById(int noteid) {
        return noteMapper.getNoteById(noteid);
    }
}
