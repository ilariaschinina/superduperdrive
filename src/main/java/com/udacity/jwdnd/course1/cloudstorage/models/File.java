package com.udacity.jwdnd.course1.cloudstorage.models;

public class File {
    /**
     * fileId INT PRIMARY KEY auto_increment,
     * filename VARCHAR(24),
     * contenttype VARCHAR(48),
     * filesize VARCHAR(12),
     * userid INT,
     * filedata BLOB,
     * foreign key (userid) references USERS(userid)
     */
    private int fileId;
    private String filename;
    private String contenttype;
    private String filesize;
    private int userid;
    private byte[] filedata;

    public File(int fileid, String filename, String contenttype, String filesize, int userid, byte[] filedata) {
        this.fileId = fileid;
        this.filename = filename;
        this.contenttype = contenttype;
        this.filesize = filesize;
        this.userid = userid;
        this.filedata = filedata;
    }

    public int getFileId() {
        return fileId;
    }

    public void setFileId(int fileId) {
        this.fileId = fileId;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getContenttype() {
        return contenttype;
    }

    public void setContenttype(String contenttype) {
        this.contenttype = contenttype;
    }

    public String getFilesize() {
        return filesize;
    }

    public void setFilesize(String filesize) {
        this.filesize = filesize;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public byte[] getFiledata() {
        return filedata;
    }

    public void setFiledata(byte[] filedata) {
        this.filedata = filedata;
    }
}