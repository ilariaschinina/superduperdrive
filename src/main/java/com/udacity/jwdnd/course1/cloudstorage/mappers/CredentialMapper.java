package com.udacity.jwdnd.course1.cloudstorage.mappers;

import com.udacity.jwdnd.course1.cloudstorage.models.Credential;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface CredentialMapper {

    @Select("SELECT * FROM CREDENTIAL")
    List<Credential> findAll();

    @Select("SELECT * FROM CREDENTIAL WHERE credentialid = #{credentialid}")
    public Credential findOne(int credentialid);

    @Select("SELECT * FROM CREDENTIAL WHERE userid = #{userid}")
    public List<Credential> findByUserId(int userid);

    @Insert("INSERT INTO CREDENTIAL (url, username, key, password, userid) VALUES (#{credential.url}, #{credential.username}, #{credential.key}, #{credential.password}, #{userid})")
    public void insertCredential(Credential credential, int userid);

    @Delete("DELETE FROM CREDENTIAL WHERE credentialid = #{credentialid}")
    public void deleteCredential(int credentialid);

    @Update("UPDATE CREDENTIAL SET url = #{url}, username = #{username}, key = #{key}, password = #{password} WHERE credentialid = #{credentialid}")
    public void updateCredential(Credential credential);
}


